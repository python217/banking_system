from pydantic import BaseModel


class CreateAccount(BaseModel):
    name: str
    initial_balance: float
    username: str
    password: str


class LoginModel(BaseModel):
    username: str
    password: str
