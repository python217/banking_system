from datetime import timedelta

from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager

from api.auth.routers import CreateAccountResource, LoginResource
from api.config.limiter import limiter
from api.config.config import JWT_SECRET_KEY
from api.wallet.routers import DepositEndpoint, TransferEndpoint, WithdrawalEndpoint

app = Flask(__name__)
api = Api(app)
app.config['JWT_SECRET_KEY'] = JWT_SECRET_KEY
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(hours=1)
jwt = JWTManager(app)

limiter.init_app(app)

# AUTH
api.add_resource(CreateAccountResource, '/auth/create_account')
api.add_resource(LoginResource, '/auth/login')

# WALLET
api.add_resource(DepositEndpoint, "/wallet/deposit")
api.add_resource(TransferEndpoint, "/wallet/transfer")
api.add_resource(WithdrawalEndpoint, "/wallet/withdrawal")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
