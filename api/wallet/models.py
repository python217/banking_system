from pydantic import BaseModel


class DepositRequest(BaseModel):
    account_id: int
    amount: float


class DepositResponse(BaseModel):
    status: str
    initial_balance: float
    deposit_amount: float


class Transfer(BaseModel):
    from_account_id: int
    to_account_id: int
    amount: float


class AccountTransferDetails(BaseModel):
    account: int
    initial_balance: float


class TransferSuccessResponse(BaseModel):
    status: str
    from_account: AccountTransferDetails
    to_account: AccountTransferDetails


class WithdrawalRequest(BaseModel):
    account_id: int
    amount: float


class WithdrawalResponse(BaseModel):
    account_id: int
    initial_balance: float
