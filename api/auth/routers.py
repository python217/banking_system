from flask import request
from flask_restful import Resource, abort
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity

from werkzeug.security import generate_password_hash, check_password_hash
from pydantic import ValidationError

from api.auth.functions import check_current_account
from api.auth.models import CreateAccount, LoginModel
from api.database.accounts import Account, accounts


class CreateAccountResource(Resource):
    """Endpoint to create a new account."""
    def post(self):
        try:
            user_data = CreateAccount.parse_raw(request.data)
        except ValidationError as e:
            raise abort(400, description="Validation Error")

        new_user = Account(
            name=user_data.name,
            initial_balance=user_data.initial_balance,
            username=user_data.username,
            password_hash=generate_password_hash(user_data.password, method='pbkdf2:sha256')
        )

        created_user = accounts.add_account(new_user)
        return created_user.dict(), 201


class LoginResource(Resource):
    def post(self):
        try:
            login_data = LoginModel.parse_raw(request.data)
        except ValidationError as e:
            raise abort(400, description="Validation Error")

        user = accounts.get_account_by_username(login_data.username)
        if user and check_password_hash(user.password_hash, login_data.password):
            access_token = create_access_token(identity=user.username)
            return {"access_token": access_token}, 200
        else:
            raise abort(401, description="Invalid username or password")
