import logging

from flask import request
from flask_restful import Resource, abort
from flask_jwt_extended import jwt_required, get_jwt_identity

from pydantic import ValidationError

from api.auth.functions import check_current_account
from api.database.accounts import Account, accounts
from api.database.deposit import deposits
from api.wallet.models import DepositRequest, Transfer, TransferSuccessResponse, AccountTransferDetails, \
    WithdrawalRequest, DepositResponse, WithdrawalResponse


class DepositEndpoint(Resource):
    @jwt_required()
    def post(self):
        current_account: Account = check_current_account(get_jwt_identity())
        try:
            deposit_request = DepositRequest.parse_raw(request.data)
        except ValidationError as e:
            raise abort(400, description="Validation Error")

        if not current_account.id == deposit_request.account_id:
            raise abort(401, description="Unable to create deposit from someone else's account")

        if not max(deposit_request.amount, 0) == deposit_request.amount:
            raise abort(400, description="Unable to create deposit with negative amount")

        logging.info(f"Deposit:\n"
                     f"Current account: {current_account}\n"
                     f"Deposit request: {deposit_request}")
        created_dep = deposits.add_deposit(current_account=current_account, amount=deposit_request.amount)
        status: str = ''
        if created_dep:
            status = 'success'

        return DepositResponse(
            status=status,
            initial_balance=current_account.initial_balance,
            deposit_amount=created_dep.amount
        ).model_dump(), 201


class TransferEndpoint(Resource):
    @jwt_required()
    def post(self):
        current_account: Account = check_current_account(get_jwt_identity())
        try:
            transfer_request = Transfer.parse_raw(request.data)
        except ValidationError as e:
            raise abort(400, description="Validation Error")

        if not max(transfer_request.amount, 0) == transfer_request.amount:
            raise abort(400, description="Unable to create a transaction with negative amount")

        if not current_account.id == transfer_request.from_account_id:
            raise abort(403, description="Unable to create a transaction from someone else's account")

        target_account = accounts.get_account_by_id(transfer_request.to_account_id)

        if not target_account:
            raise abort(401, description="The target payment account does not exist")

        logging.info(f"Transfer: {transfer_request}\n"
                     f"Current account: {current_account}\n"
                     f"Target account: {target_account}")

        status = accounts.transaction_between_accounts(current_account=current_account,
                                                       target_account=target_account,
                                                       amount=transfer_request.amount)

        return TransferSuccessResponse(status=status,
                                       from_account=AccountTransferDetails(
                                           account=current_account.id,
                                           initial_balance=current_account.initial_balance
                                       ),
                                       to_account=AccountTransferDetails(
                                           account=target_account.id,
                                           initial_balance=target_account.initial_balance
                                       )).model_dump(), 200


class WithdrawalEndpoint(Resource):
    @jwt_required()
    def post(self):
        current_account: Account = check_current_account(get_jwt_identity())
        try:
            withdrawal_request = WithdrawalRequest.parse_raw(request.data)
        except ValidationError as e:
            raise abort(400, description="Validation Error")

        if not current_account.id == withdrawal_request.account_id:
            raise abort(403, description="Unable withdrawal from someone else's account")

        if not max(withdrawal_request.amount, 0) == withdrawal_request.amount:
            raise abort(400, description="Unable to withdrawal with negative amount")

        logging.info(f"REQUEST WITHDRAWAL: {withdrawal_request.amount}\n"
                     f"Withdrawal: {withdrawal_request}\n"
                     f"Current account: {current_account}\n")

        withdrawal = accounts.withdraw(current_account=current_account, amount=withdrawal_request.amount)

        if not withdrawal:
            logging.error(f"Unable to withdraw: {withdrawal_request.amount}\n"
                          f"Withdrawal: {withdrawal_request}\n"
                          f"Current account: {current_account}\n")
            raise abort(400, description="Unable to withdraw")

        return WithdrawalResponse(
            account_id=current_account.id,
            initial_balance=current_account.initial_balance
        ).model_dump(), 200
