Python API Service Homework: Banking
System
Introduction
In this homework assignment, you will be implementing a simple banking system API
service using Python. The API will allow users to create accounts, deposit money,
withdraw money, and send money between accounts.