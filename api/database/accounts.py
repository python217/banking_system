import logging
from typing import Optional

from pydantic import BaseModel
from flask_restful import abort


class Account(BaseModel):
    id: Optional[int] = 1
    name: str
    initial_balance: float
    username: str
    password_hash: str


class Accounts:
    def __init__(self):
        # I could use dictionaries, but I don't like this approach for further logic.
        self.accounts = []

    def add_account(self, account: Account) -> Account:
        if not self.get_account_by_username(account.username):
            if self.accounts:
                last_id = self.accounts[-1].id
                account.id = last_id + 1
            self.accounts.append(account)
            return account
        else:
            raise abort(409, description="An account with this username already exists")

    def get_account_by_id(self, account_id: int) -> Optional[Account]:
        return next((account for account in self.accounts if account.id == account_id), None)

    def get_account_by_username(self, username: str) -> Optional[Account]:
        return next((user for user in self.accounts if user.username == username), None)

    @staticmethod
    def transaction_between_accounts(current_account: Account, target_account: Account, amount: float) -> str:
        if amount > current_account.initial_balance:
            logging.info(f"Transfer: cannot create transaction more than current account balance: {amount}\n"
                         f"Current account: {current_account}\n"
                         f"Target account: {target_account}")
            raise abort(400, description="You cannot create transaction more than your balance")

        initial_balance_current_account = current_account.initial_balance
        initial_balance_target_account = target_account.initial_balance
        try:
            current_account.initial_balance -= amount
            target_account.initial_balance += amount
        except Exception as e:
            current_account.initial_balance = initial_balance_current_account
            target_account.initial_balance = initial_balance_target_account
            logging.error(f"Transfer: cannot transfer: {e}\n"
                          f"Current account: {current_account}\n"
                          f"Target account: {target_account}"
                          f"Amount: {amount}")
            raise abort(400, description="Cannot transfer. Try later")
        logging.info(f"Transfer:\n"
                     f"Current account: {current_account}\n"
                     f"Target account: {target_account}"
                     f"Amount: {amount}"
                     f"Status: success\n")
        return "success"

    @staticmethod
    def withdraw(current_account: Account, amount: float) -> bool:
        if amount > current_account.initial_balance:
            logging.error(f"Unable to withdraw: {amount}\n"
                          f"Current account: {current_account}\n")
            raise abort(400, description="You cannot withdrawal more than your balance")

        initial_balance_current_account = current_account.initial_balance
        try:
            current_account.initial_balance -= amount
        except Exception as e:
            current_account.initial_balance = initial_balance_current_account
            logging.error(f"Unable to withdraw: {e}\n"
                          f"Amount: {amount}\n"
                          f"Current account: {current_account}\n")
            raise abort(400, description="Cannot withdraw. Try later")

        logging.info(f"Withdraw success: {amount}\n"
                     f"Current account: {current_account}\n")

        return True


accounts = Accounts()
