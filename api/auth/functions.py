from api.database.accounts import accounts
from flask_restful import abort


def check_current_account(current_account_username):
    if account := accounts.get_account_by_username(current_account_username):
        return account
    else:
        raise abort(401, description="Account not found")
