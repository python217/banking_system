import logging
from typing import Optional

from pydantic import BaseModel

from api.database.accounts import Account

from flask_restful import abort


class Deposit(BaseModel):
    id: Optional[int] = 1
    account_id: int
    amount: float


class Deposits:
    def __init__(self):
        # I could use dictionaries, but I don't like this approach for further logic.
        self.deposits = []

    def add_deposit(self, current_account: Account, amount: float) -> Deposit:
        if amount > current_account.initial_balance:
            logging.error(f"Unable deposit: deposit more than account balance\n"
                          f"Account: {current_account}\n"
                          f"Deposit Amount: {amount}")
            raise abort(400, description="You cannot deposit more than your balance")

        initial_balance_current_account = current_account.initial_balance
        try:
            current_account.initial_balance -= amount
            deposit = Deposit(account_id=current_account.id, amount=amount)
            if self.deposits:
                last_id = self.deposits[-1].id
                deposit.id = last_id + 1
            self.deposits.append(deposit)
            logging.info(f"Deposit: {amount}\n"
                         f"Account: {current_account}\n"
                         f"Status: success")
            return deposit
        except Exception as e:
            logging.error(f"Deposit: Failed to create deposit: {e}\n"
                          f"Account: {current_account}\n"
                          f"Deposit Amount: {amount}")
            current_account.initial_balance = initial_balance_current_account
            raise abort(400, description="Failed to create deposit")


deposits = Deposits()
